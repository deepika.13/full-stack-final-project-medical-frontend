import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {

  constructor(private httpClient: HttpClient,private sanitizer:DomSanitizer) { }

  selectedFile: File;
    retrievedImage: any;
    base64Data: any;
    retrieveResonse: any;
    message: string;
    imageName: any;

    //Gets called when the user selects an image

  public onFileChanged(event) {
    //Select File
    this.selectedFile = event.target.files[0];
  }

   //Gets called when the user clicks on submit to upload the image

onUpload() {
  console.log(this.selectedFile);
  //FormData API provides methods and properties to allow us easily prepare form data to be sent with POST HTTP requests.
  const uploadImageData = new FormData();
  uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);
  //Make a call to the Spring Boot Application to save the image
  this.httpClient.post('http://localhost:9000/image/upload', uploadImageData, { observe: 'response' })
    .subscribe((response) => {
      if (response.status === 200) {
        this.message = 'Image uploaded successfully';
      } else {
        this.message = 'Image not uploaded successfully';
      }
    }
    );
}

  //Gets called when the user clicks on retieve image button to get the image from back end


  
  getImage() {


    //Make a call to Sprinf Boot to get the Image Bytes.
this.httpClient.get('http://localhost:9000/image/get/'+"/"+this.imageName)
  .subscribe(
    res => {
      this.retrieveResonse = res;
      console.log(res);
  //       console.log(data.Image);
      this.base64Data = this.retrieveResonse.picByte;
      console.log(this.base64Data);
  //       console.log(data.Image);
      this.retrievedImage = 'data:image/jpeg;base64,'+this.base64Data;
    }
  );
}
  //Make a call to Sprinf Boot to get the Image Bytes.
  // this.httpClient.get('http://localhost:9000/image/get'+"/"+this.imageName)
  //   .subscribe( 
  //     ((data: any) => {


  //       let objectURL = 'data:image/png;base64,' + data.Image;
  //       console.log(objectURL);
  //       console.log(data.Image);

  //       this.retrievedImage = this.sanitizer.bypassSecurityTrustUrl(objectURL);  
  //       console.log(this.retrievedImage);
        // this.retrieveResonse = res;
        // this.base64Data = this.retrieveResonse.picByte;
        // console.log(this.retrieveResonse.picByte);

        // console.log(this.base64Data);
        // this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
      //})
  // )

  // return this.httpClient.get('http://localhost:9000/image/get'+"/"+ this.imageName, {  
  //   responseType: 'blob' 
  // });  

    
    ngOnInit() {
  }
  //  downloadImage(image: string): Observable < Blob > {  
  //   return this.http.get(this.url + '/GetImage?image=' + image, {  
  //       responseType: 'blob'  
  //   });  

}
