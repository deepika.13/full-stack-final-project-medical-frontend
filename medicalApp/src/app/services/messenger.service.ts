import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class MessengerService {

 subject = new Subject(); // to trigger an event


  constructor() { }




  
  sendMsg(product){ //one product

    console.log("inside sendMsg"+product);
    
    this.subject.next(product); // send that product

  }

  getMsg(){

return this.subject.asObservable();

  }


}
