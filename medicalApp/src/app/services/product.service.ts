import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url1: string;
byte:any;

  constructor(private http:HttpClient) {
this.url1 = "http://localhost:9000/image/get"
  }

getProducts(): Observable<Product[]>{
  return this.http.get<Product[]>(this.url1);//http get returns us an observable

}

}
