import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Orders } from '../models/orders';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CartService {
  cartUrl: string;
  name: string;
  orderUrl: string;


  constructor(private http: HttpClient) {
    this.orderUrl = "http://localhost:9000/getCart";
    this.cartUrl = 'http://localhost:9000/order';

  }


  addProductToCart(order: Orders): Observable<String> {
    console.log("inside addProductToCart posting method " + (Product.toString()));

    return this.http.post<String>(this.cartUrl,order);

  }

  getCartItems(): Observable<Orders[]> {
    //   Mapping the obtained result to our CartItem props. (pipe() and map())
    console.log("entered  loadCartItems in url" + this.http.get<Orders[]>(this.orderUrl));
    //return this.http.get<Orders[]>(this.orderUrl); //change now
    return this.http.get<Orders[]>(this.orderUrl);


    //     let cartItems: Orders[] = [];

    //     console.log("item is ...");

    //     for (let item of result) {
    //       console.log("item is ..."+item);
    //       let productExists = false

    //       for (let i in cartItems) {
    //         console.log(i);
    //       //  console.log("cartItems[i].productId"+cartItems[i].productId);

    //      //   if (cartItems[i].productId === item.product.id) 
    //      {
    //         //  cartItems[i].qty++
    //       //    productExists = true
    //         //  break;
    //         }
    //       }

    //       if (!productExists) {
    //       //  cartItems.push(new CartItem(item.id, item.product));
    //       }
    //     }

    //     return cartItems;
    //   })
    // );
  }


}
