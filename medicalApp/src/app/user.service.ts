import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user'


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url: string;
  private loginUrl : string;

  constructor(private http: HttpClient) {
    this.url = "http://localhost:9000/register";
    this.loginUrl="http://localhost:9000/login";

  }

  public register(user: User): Observable<String> {
    console.log("enter register");
    return this.http.post<String>(this.url,user);

  }

  public login(user: User): Observable<User[]> {
    console.log("enter login");
    return this.http.post<User[]>(this.loginUrl,user);

  }
}
