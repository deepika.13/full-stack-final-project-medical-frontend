import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public user: User;

  constructor(private service: UserService, public router: Router) {
    this.user = new User();
  }


  register() {
    console.log("entered register component");

    this.service.register(this.user).subscribe(data =>
       {
         this.user;
     this.user=new User();
      this.router.navigate(['/login']);
    })



  }

  gotoHome(){
    this.router.navigate(['/www.google.com']);  // define your component where you want to go
}

  ngOnInit() {
  }

}
