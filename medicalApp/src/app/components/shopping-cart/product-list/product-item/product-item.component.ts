import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/models/product';
import { MessengerService } from 'src/app/services/messenger.service';
import { CartService } from 'src/app/services/cart.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Orders } from 'src/app/models/orders';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {

  @Input() productItem: Product; //coming as an input frpm Parent component Product List

  order: Orders;


  constructor(private msg: MessengerService, public cartService: CartService, private http: HttpClient) {
    console.log("entered constructor");
  }


  cartUrl = 'http://localhost:9000/order';


  ngOnInit(): void {

    console.log(this.productItem.name);
    console.log(this.productItem.price);


  }


  OrderedData() {
    this.order = new Orders(this.productItem.id, this.productItem.name, this.productItem.price, this.productItem.id);

    console.log("order is" + this.order);

  }

  handleAddToCart() {

    this.OrderedData();
    this.cartService.addProductToCart(this.order).subscribe(() => {

      console.log("order is" + this.order);
      console.log("entered handleAddToCart")

      this.msg.sendMsg(this.order)
      console.log(this.order)


    })


  }



}
