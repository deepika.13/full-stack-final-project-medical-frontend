import { Component, OnInit, Input, SystemJsNgModuleLoader } from '@angular/core';
import { MessengerService } from 'src/app/services/messenger.service';
import { Product } from 'src/app/models/product';
import { CartService } from 'src/app/services/cart.service';
import { Orders } from 'src/app/models/orders';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {


  public cartItems = [];

  cartTotal = 0 ;
    // {id:2,productID:2,productName:'abc',qty:4,price:100}, // if api is not there
  orders =[];

  ord : Orders[] = [];;

  constructor(private msg: MessengerService, private cartService: CartService) { }

  ngOnInit(): void {
    this.handleSubscription();
    this.loadCartItems();

  }



  handleSubscription() {
    this.msg.getMsg().subscribe((order: Orders) => { // when added to cart get the message from product
      this.loadCartItems();
    })
  }


  loadCartItems() {
    this.cartService.getCartItems().subscribe((items: Orders[]) => {
      this.cartItems = items;

     // this.cartTotalAmt();
     this.ord = [];

    console.log("items   "+items.length)
    this.cartTotalAmt();

    //   this.cartItems.forEach(item => {
    //     this.cartTotal += (item.qty * item.price)
  
  
     // })
    }      
    )
   // console.log(this.cartItems.length);

    //this.cartTotalAmt();
  }

  cartTotalAmt() {
    console.log(this.cartItems.length);

    console.log("entered cartTotalAmt");
    this.cartTotal = 0
    this.cartItems.forEach(item => {
      this.ord.push(new Orders(item.oid,item.name,item.price,item.qty))
      this.cartTotal += (item.qty * item.price)

      console.log(  "cartTotalAmt ord length" + this.ord[0].name);

     })

  }

}
