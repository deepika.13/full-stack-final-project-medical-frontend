import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user: User;
  public count: number;

  constructor(private router: Router, private service: UserService) {
    this.user = new User();

  }

  ngOnInit() {



  }

  login() {
    console.log("entered login component");

    this.service.login(this.user).subscribe((data: User[]) => {

      console.log("entered login component" + data);

      if (data != null) {
        console.log("entered if");
        this.router.navigate(['/shopping-cart']);

      }
      else {
        console.log("entered else");

        this.count = 1;
        this.user = new User();

      }
      // this.user=new User();
      //  this.router.navigate(['/login']);
    })


  }



}
