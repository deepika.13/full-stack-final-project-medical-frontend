import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { ShopComponent } from './shop/shop.component';
import { LoginComponent } from './login/login.component';
import { PaymentComponent } from './components/payment/payment.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';



const routes: Routes = [
  {path:'',redirectTo: '/shopping-cart' , pathMatch: 'full'},
  {path:"shop",component:ShopComponent},
  {path:"shopping-cart" , component:ShoppingCartComponent},
  
  {path:"login",component:LoginComponent},
  {path:"payment",component:PaymentComponent},

 // {path:"user",component:UserComponent}
 {
   path:"user",component:UserComponent,
   children:[
{
path:'login',component:LoginComponent


}
   ]
  
  }



];

@NgModule({
  imports: [RouterModule.forRoot(routes),RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
